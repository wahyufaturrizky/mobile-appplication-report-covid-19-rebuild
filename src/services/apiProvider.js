import API from './axiosConfig';
import * as url from './urls';

export default {
  //********************* */
  //auth
  loginProcess: params => {
    console.log('api', API);
    return API(url.LOGIN_PROCESS, {
      method: 'POST',
      head: {
        'Content-Type': 'application/json',
      },
      body: {
        ...params,
      },
    });
  },

  //********************* */
};
