import {
  LOGIN_SUCCESS,
  LOGIN_FAILED,
  RESET_LOGIN_FAILED
} from '../types';
import api from '../../../services/apiProvider'

export const loginProcess = params => {
  return dispatch => {
    api
      .loginProcess(params)
      .then(response => {
        console.log(response);
        dispatch({type: LOGIN_SUCCESS, response});
      })
      .catch(error => {
        const response = error.response.data;
        console.log('err login', response);
        dispatch({type: LOGIN_FAILED, response});
      });
  };
};

export const reset_login_failed = () => ({ type: RESET_LOGIN_FAILED })