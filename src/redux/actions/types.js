//account
export const GET_PROFILE = 'GET_PROFILE';

// auth
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAILED = 'LOGIN_FAILED';
export const RESET_LOGIN_FAILED = 'RESET_LOGIN_FAILED';