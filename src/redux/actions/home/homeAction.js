import {HOME_DATA, HOME_DATA_ERROR} from '../types';
import api from '../../../services/apiProvider';

export const getHomeData = () => {
  return dispatch => {
    api
      .getHomeData()
      .then(response => {
        console.log('homeData', response);
        dispatch({type: HOME_DATA, response});
      })
      .catch(error => {
        const response = error.response.data;
        console.log('homeData ERROR', response);
        dispatch({type: HOME_DATA_ERROR, response});
      });
  };
};
