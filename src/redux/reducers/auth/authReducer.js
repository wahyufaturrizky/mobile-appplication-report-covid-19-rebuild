import {
  LOGIN_SUCCESS,
  LOGIN_FAILED,
  RESET_LOGIN_FAILED
} from '../../actions/types';
// import {authInitialState as initialState} from '../initialState';

const initialState = {
  isLoggedIn: false,
  userData: null,
  loginFailed: false
}

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN_SUCCESS:
      return {
        ...state,
        isLoggedIn: true,
        userData: action.response.payload,
        loginSuccess: true,
      };
    case LOGIN_FAILED:
      return {
        ...state,
        loginFailed: true
      }
    case RESET_LOGIN_FAILED:
      return {
        ...state,
        loginFailed: false
      }
    default:
      return state;
  }
};

export default authReducer;
