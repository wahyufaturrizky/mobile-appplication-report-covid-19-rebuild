const authInitialState = {
  isLoggedIn: false,
  userData: {},
};

export {
  authInitialState,
};
