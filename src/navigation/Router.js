import React from 'react';
import {Router, Scene, } from 'react-native-router-flux';
import { Icon } from 'react-native-elements';
import Colors from '../styles/colors'

import Intro from '../containers/Intro';
import Home from '../containers/home/Home';
import Reminder from '../containers/reminder/Reminder';
import Screening from '../containers/screening/Screening';
import Profile from '../containers/profile/Profile';

//auth
import Login from '../containers/auth/Login'

const TabIcon = ({iconName, focused}) => {
	return (
		<Icon name={iconName} type="antdesign" iconStyle={{ color: focused ? Colors.yellow4: Colors.grey1 }}/>
	)
};


class Routing extends React.Component {
    render() {
        return (
         <Router>
            <Scene key="root">   
               <Scene key="tabbar" tabs showLabel={true} inactiveTintColor={Colors.grey1} activeTintColor={Colors.yellow4} hideNavBar>
                  <Scene key="home" component={Home} title="Home" iconName="home" icon={TabIcon} hideNavBar/>
                  <Scene key="reminder" component={Reminder} title="Reminder" iconName="notification" icon={TabIcon} hideNavBar/>
                  <Scene key="screening" component={Screening} title="Screening" iconName="solution1" icon={TabIcon} hideNavBar/>
                  <Scene key="profile" component={Profile} title="Profile" iconName="user" icon={TabIcon} hideNavBar/>
               </Scene>
               <Scene key="intro" component={Intro} hideNavBar/>
               <Scene key="login" component={Login} initial hideNavBar/>
            </Scene>
      </Router>
        );
    }
}

export default Routing;