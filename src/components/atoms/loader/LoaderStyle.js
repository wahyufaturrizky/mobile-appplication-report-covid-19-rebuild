import {StyleSheet} from 'react-native';
import Colors from '../../../styles/colors';

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.white,
    opacity: 0.5,
    width: '100%',
    height: '100%',
    // alignSelf: 'center',
    // flexDirection: 'column',
  },
});

export default styles;
