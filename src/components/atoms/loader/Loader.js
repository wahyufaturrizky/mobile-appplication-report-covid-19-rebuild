import React from 'react';
import {
  View,
  Modal,
  ActivityIndicator,
  Platform,
  SafeAreaView,
} from 'react-native';
import Colors from '../../../styles/colors';
import styles from './LoaderStyle';

const onIOS = Platform.OS === 'ios';

const Loader = ({show, type}) => {
  return (
    <Modal visible={show} transparent={true}>
      <SafeAreaView>
        <View style={styles.container}>
          <ActivityIndicator
            size="large"
            // color={Colors.primaryMain}
            color="red"
            style={{fontSize: 30}}
          />
        </View>
      </SafeAreaView>
    </Modal>
  );
};

export default Loader;
