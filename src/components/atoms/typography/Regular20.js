import React from 'react';
import {Text} from 'react-native';

const Regular20 = ({color, children, style}) => {
  return (
    <Text
      style={{
        color: color,
        fontSize: 20,
        // fontFamily: 'Poppins-Regular',
        ...style,
      }}>
      {children}
    </Text>
  );
};

export default Regular20;
