import React from 'react';
import {Text} from 'react-native';

const SemiBold14 = ({color, children, style, onPress}) => {
  return (
    <Text
      onPress={onPress}
      style={{
        color: color,
        fontSize: 14,
        // fontFamily: 'Poppins-SemiBold',
        ...style,
      }}>
      {children}
    </Text>
  );
};

export default SemiBold14;
