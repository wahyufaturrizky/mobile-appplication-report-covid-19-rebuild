import Bold10 from './Bold10';
import Bold12 from './Bold12';
import Bold14 from './Bold14';
import Bold16 from './Bold16';
import Bold18 from './Bold18';
import Bold24 from './Bold24';
import Bold32 from './Bold32';
import Medium12 from './Medium12';
import Medium14 from './Medium14';
import Medium16 from './Medium16';
import Regular10 from './Regular10';
import Regular12 from './Regular12';
import Regular14 from './Regular14';
import Regular16 from './Regular16';
import Regular18 from './Regular18';
import Regular20 from './Regular20';
import Regular24 from './Regular24';
import Regular30 from './Regular30';

export {
  Bold10,
  Bold12,
  Bold14,
  Bold16,
  Bold18,
  Bold24,
  Bold32,
  Medium12,
  Medium14,
  Medium16,
  Regular10,
  Regular12,
  Regular14,
  Regular16,
  Regular18,
  Regular20,
  Regular24,
  Regular30
};
