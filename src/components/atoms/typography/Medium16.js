import React from 'react';
import {Text} from 'react-native';

const Medium16 = ({color, children, style}) => {
  return (
    <Text
      style={{
        color: color,
        fontSize: 16,
        // fontFamily: 'Poppins-Medium',
        ...style,
      }}>
      {children}
    </Text>
  );
};

export default Medium16;
