import React from 'react';
import {Text} from 'react-native';

const Bold16 = ({color, children, style, textProps}) => {
  return (
    <Text
      {...textProps}
      style={{
        color: color,
        fontSize: 16,
        // fontFamily: 'Poppins-Bold',
        ...style,
      }}>
      {children}
    </Text>
  );
};

export default Bold16;
