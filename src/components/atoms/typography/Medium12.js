import React from 'react';
import {Text} from 'react-native';

const Medium12 = ({color, children, style}) => {
  return (
    <Text
      style={{
        color: color,
        fontSize: 12,
        // fontFamily: 'Poppins-Medium',
        ...style,
      }}>
      {children}
    </Text>
  );
};

export default Medium12;
