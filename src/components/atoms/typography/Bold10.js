import React from 'react';
import {Text} from 'react-native';

const Bold10 = ({color, children, style}) => {
  return (
    <Text
      style={{
        color: color,
        fontSize: 10,
        // fontFamily: 'Poppins-Bold',
        ...style,
      }}>
      {children}
    </Text>
  );
};

export default Bold10;
