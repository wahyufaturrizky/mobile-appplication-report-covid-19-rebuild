import React from 'react';
import {Text} from 'react-native';

const Regular14 = ({color, children, style, textProps}) => {
  return (
    <Text
      {...textProps}
      style={{
        color: color,
        fontSize: 14,
        // fontFamily: 'Poppins-Regular',
        ...style,
      }}>
      {children}
    </Text>
  );
};

export default Regular14;
