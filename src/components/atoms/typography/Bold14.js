import React from 'react';
import {Text} from 'react-native';

const Bold14 = ({color, children, style, onPress}) => {
  return (
    <Text
      onPress={onPress}
      style={{
        color: color,
        fontSize: 14,
        // fontFamily: 'Poppins-Bold',
        ...style,
      }}>
      {children}
    </Text>
  );
};

export default Bold14;
