import React from 'react';
import {Text} from 'react-native';

const Regular30 = ({color, children, style}) => {
  return (
    <Text
      style={{
        color: color,
        fontSize: 30,
        // fontFamily: 'Poppins-Regular',
        ...style,
      }}>
      {children}
    </Text>
  );
};

export default Regular30;
