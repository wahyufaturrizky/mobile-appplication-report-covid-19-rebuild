import React from 'react';
import {Text} from 'react-native';

const Medium14 = ({color, children, style, textProps}) => {
  return (
    <Text
      {...textProps}
      style={{
        color: color,
        fontSize: 14,
        // fontFamily: 'Poppins-Medium',
        ...style,
      }}>
      {children}
    </Text>
  );
};

export default Medium14;
