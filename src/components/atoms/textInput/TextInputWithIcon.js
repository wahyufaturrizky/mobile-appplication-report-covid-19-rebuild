import React, {useState, useEffect} from 'react';
import {View, TextInput, Animated, TouchableOpacity} from 'react-native';
import styles from './TextInputWithIconStyle';
import Icon from 'react-native-vector-icons/Entypo';
import Colors from '../../../styles/colors';
import {Regular14} from '../typography/typography';

const TextInputWithIcon = ({
  icon,
  iconColor,
  iconPosition,
  placeholder,
  placeholderTextColor = Colors.white,
  textColor,
  label,
  style,
  focusBorderColor,
  floatLabel = false,
  clickableIcon = false,
  onPressIcon,
  inputProps,
}) => {
  const position = new Animated.Value(0);
  useEffect(() => {
    Animated.timing(position, {
      toValue: 1,
      duration: 300,
    }).start();
  });
  const [focus, setFocus] = useState(false);

  return (
    <View
      style={[
        style,
        focus && focusBorderColor
          ? {borderColor: focusBorderColor, borderWidth: 1}
          : null,
      ]}>
      {floatLabel && focus && (
        <Animated.Text
          style={[
            styles.floatLabel,
            {
              color: focusBorderColor,
              top: -10,
              // top: position.interpolate({
              //   inputRange: [0, 1],
              //   outputRange: [14, -10],
              // }),
            },
          ]}>
          {label}
        </Animated.Text>
      )}
      {label && !floatLabel && <Regular14 color={iconColor}>{label}</Regular14>}
      <View style={styles.container}>
        {iconPosition === 'left' && (
          <View style={{width: '10%'}}>
            {clickableIcon ? (
              <TouchableOpacity onPress={onPressIcon}>
                <Icon
                  size={20}
                  name={icon}
                  style={{color: iconColor, marginRight: 10}}
                />
              </TouchableOpacity>
            ) : (
              <Icon
                size={20}
                name={icon}
                style={{color: iconColor, marginRight: 10}}
              />
            )}
          </View>
        )}
        <TextInput
          onFocus={() => setFocus(true)}
          onBlur={() => setFocus(false)}
          style={[styles.inputText, {color: textColor}]}
          placeholderTextColor={placeholderTextColor}
          placeholder={placeholder && !focus ? placeholder : ''}
          {...inputProps}
        />
        {iconPosition === 'right' && (
          <View style={{width: '10%'}}>
            {clickableIcon ? (
              <TouchableOpacity onPress={onPressIcon}>
                <Icon
                  size={20}
                  name={icon}
                  style={{color: iconColor, marginLeft: 10}}
                />
              </TouchableOpacity>
            ) : (
              <Icon
                size={20}
                name={icon}
                style={{color: iconColor, marginLeft: 10}}
              />
            )}
          </View>
        )}
      </View>
    </View>
  );
};

export default TextInputWithIcon;
