import React from 'react';
import {View, TextInput} from 'react-native';
import styles from './NormalTextInputStyle';
import {Regular12} from '../typography/typography';
import Colors from '../../../styles/colors';
import Icon from 'react-native-vector-icons/AntDesign';

const NormalTextInput = ({
  onChangeText,
  value,
  placeholder,
  placeholderTextColor,
  editable,
  keyboardType,
  iconRight,
  colorBackground,
  iconColor,
  radius,
  style,
}) => {
  return (
    <View
      style={[
        styles.viewInputStyle,
        style,
        {
          borderRadius: radius,
          backgroundColor: colorBackground
            ? colorBackground
            : Colors.primaryCard,
        },
      ]}>
      <TextInput
        style={styles.textInputStyle}
        onChangeText={value => onChangeText(value)}
        value={value}
        placeholder={placeholder}
        placeholderTextColor={placeholderTextColor}
        editable={editable}
        keyboardType={keyboardType}
      />
      <Icon name={iconRight} size={25} color={iconColor} />
    </View>
  );
};

export default NormalTextInput;
