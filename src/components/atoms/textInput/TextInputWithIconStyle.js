import {StyleSheet} from 'react-native';
import Colors from '../../../styles/colors';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    overflow: 'hidden',
    // borderBottomColor: Colors.white,
    // borderBottomWidth: 1,
    justifyContent: 'space-between',
  },
  inputText: {
    color: 'black',
    paddingVertical: 9,
    width: '90%',
  },
  floatLabel: {
    color: 'red',
    position: 'absolute',
    // top: -10,
    left: 10,
    backgroundColor: Colors.white,
    paddingHorizontal: 3,
  },
});

export default styles;
