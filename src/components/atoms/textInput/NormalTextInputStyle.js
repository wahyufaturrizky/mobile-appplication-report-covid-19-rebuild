import {StyleSheet} from 'react-native';
import Colors from '../../../styles/colors';

const styles = StyleSheet.create({
  container: {},
  viewInputStyle: {
    paddingHorizontal: 20,
    borderRadius: 2,
    alignItems: 'center',
    alignContent: 'center',
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderRadius: 25,
    backgroundColor: Colors.primaryCard,
    paddingVertical: Platform.OS == 'ios' ? 10:0, //pake ini aja gan
  },
  textInputStyle: {
    width: '80%',
  },
});

export default styles;
