import React from 'react';
import {View, TextInput} from 'react-native';
import styles from './LabelOrBorderInputStyle';
import {Regular12} from '../typography/typography';
import Colors from '../../../styles/colors';
import Icon from 'react-native-vector-icons/AntDesign';

const BorderInput = ({
  textLabel,
  colorText,
  color,
  onChangeText,
  value,
  placeholder,
  placeholderTextColor,
  editable,
  keyboardType,
  iconLeft,
  iconRight,
  type,
  width,
  paddingBottom,
  borderBottomColor,
  borderBottomWidth,
  margonTop,
}) => {
  return (
    <View
      style={[
        {
          width: width,
          marginTop: margonTop ? margonTop : 0,
          paddingBottom: paddingBottom ? paddingBottom : 0,
          borderBottomColor: borderBottomColor ? borderBottomColor : null,
          borderBottomWidth: borderBottomWidth ? borderBottomWidth : 0,
        },
        styles.container,
      ]}>
      {textLabel && (
        <View style={[styles.textLabel]}>
          <Regular12 color={colorText}>{textLabel}</Regular12>
        </View>
      )}
      <View style={[styles.content, type ? styles[type] : false]}>
        {iconLeft && (
          <Icon name={iconLeft} size={25} color={Colors.primaryCard} />
        )}
        <TextInput
          style={[
            iconLeft || iconRight ? styles.textInput : styles.borderInput,
            {color: color ? color : '#000'},
          ]}
          onChangeText={onChangeText}
          value={value}
          placeholder={placeholder}
          placeholderTextColor={placeholderTextColor}
          editable={editable}
          keyboardType={keyboardType}
        />
        {iconRight && (
          <Icon name={iconRight} size={25} color={Colors.primaryCard} />
        )}
      </View>
    </View>
  );
};

export default BorderInput;
