import {StyleSheet} from 'react-native';
import Colors from '../../../styles/colors';

const styles = StyleSheet.create({
  label: {
    color: Colors.primaryMain,
    marginTop: 7,
  },
});

export default styles;
