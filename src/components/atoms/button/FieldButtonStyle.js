import {StyleSheet} from 'react-native';
import Colors from '../../../styles/colors';

const styles = StyleSheet.create({
  button: {
    paddingVertical: 9,
    paddingHorizontal: 9,
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center',
    flexDirection: 'row',
  },
  textButton: {
    color: Colors.primaryCard,
    textAlignVertical: 'center',
    textAlign: 'center',
  },
});

export default styles;
