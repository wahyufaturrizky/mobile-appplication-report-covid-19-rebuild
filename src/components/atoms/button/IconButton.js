import React from 'react';
import {TouchableOpacity} from 'react-native';
import styles from './IconButtonStyle';
import Icon from '../icons/Icon';
import Regular12 from '../typography/Regular12';

const IconButton = ({onPress, iconName, style, iconStyle, label, newIcon}) => {
  return (
    <TouchableOpacity style={style} onPress={onPress}>
      {newIcon ? newIcon : <Icon style={iconStyle} name={iconName} />}

      {label && <Regular12 style={styles.label}>{label}</Regular12>}
    </TouchableOpacity>
  );
};

export default IconButton;
