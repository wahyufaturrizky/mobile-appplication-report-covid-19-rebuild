import React from 'react';
import {TouchableOpacity, View} from 'react-native';
import styles from './ButtonStyle';

const Button = ({
  backgroundColor,
  children,
  style,
  onPress,
  rounded,
  width,
}) => {
  return (
    <TouchableOpacity activeOpacity={0.9} onPress={onPress}>
      <View
        style={[
          styles.default,
          rounded ? styles.rounded : null,
          {
            backgroundColor: backgroundColor ? backgroundColor : null,
            width: width,
          },
          style,
        ]}>
        {children}
      </View>
    </TouchableOpacity>
  );
};

export default Button;
