import React from 'react';
import {TouchableOpacity} from 'react-native';
import styles from './FieldButtonStyle';
import Colors from '../../../styles/colors';
import {Regular14, Bold14} from '../typography/typography';

const FieldButton = ({
  inactive,
  text,
  onPress,
  colorBackground,
  colorText,
  style,
  icon,
  marginTop,
  disabled,
  textBold,
}) => {
  return (
    <TouchableOpacity
      disabled={disabled}
      onPress={onPress}
      activeOpacity={disabled ? 0.9 : 0.2}
      // eslint-disable-next-line react-native/no-inline-styles
      style={{
        marginTop: marginTop ? marginTop : 10,
        backgroundColor: colorBackground ? colorBackground : Colors.primaryMain,
        ...style,
        ...styles.button,
      }}>
      {icon ? icon : null}
      {textBold ? (
        <Bold14 color={colorText}>{text}</Bold14>
      ) : (
        <Regular14 color={colorText}>{text}</Regular14>
      )}
    </TouchableOpacity>
  );
};

export default FieldButton;
