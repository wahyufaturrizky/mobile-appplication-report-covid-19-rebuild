import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  default: {
    paddingHorizontal: 13,
    paddingVertical: 13,
    alignSelf: 'flex-start',
    backgroundColor: 'grey',
    borderRadius: 3,
    height: undefined,
    width: undefined,
  },
  rounded: {
    borderRadius: 100,
  },
});

export default styles;
