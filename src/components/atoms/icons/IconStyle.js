import {StyleSheet} from 'react-native';
import Colors from '../../../styles/colors';
const styles = StyleSheet.create({
  icon: {
    width: 25,
    height: 25,
    tintColor: Colors.primaryCard,
  },
});

export default styles;
