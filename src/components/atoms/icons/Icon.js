import React from 'react';
import {Image} from 'react-native';
import sourceIcon from './types';
import styles from './IconStyle';

const Icon = ({name, width, height, color, style, transform}) => {
  return (
    <Image
      source={sourceIcon(name)}
      style={[
        styles.icon,
        {
          transform: [{rotate: `${transform ? transform : 0}deg`}],
          width: width ? width : 25,
          height: height ? height : 25,
          tintColor: color ? color : null,
        },
        style,
      ]}
      resizeMode="contain"
    />
  );
};

export default Icon;
