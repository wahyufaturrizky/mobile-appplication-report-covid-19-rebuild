import calender_ic from '../../../assets/icons/calendar_ic.png';
import left_arrow from '../../../assets/icons/ic_left_arrow.png';

const iconSource = name => {
  let icon;
  switch (name) {
    case 'calender_ic':
      icon = calender_ic;
      break;
    case 'left_arrow':
      icon = left_arrow;
      break;
    default:
      icon = left_arrow;
  }
  return icon;
};
export default iconSource;
