import React, {useState} from 'react';
import {
  TouchableOpacity,
  Text,
  View,
  Modal,
  SafeAreaView,
  FlatList,
  TouchableWithoutFeedback,
} from 'react-native';
import styles from './PickerSelectStyle';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Colors from '../../../styles/colors';
import {Regular14} from '../typography/typography';

const PickerSelect = ({
  selectedValue,
  items,
  onSelectedItem,
  style,
  placeholder,
  disabled,
}) => {
  const [visible, setVisible] = useState(false);
  return (
    <TouchableWithoutFeedback
      disabled={disabled}
      onPress={() => setVisible(true)}>
      <View>
        <View style={[styles.container, {opacity: disabled ? 0.3 : 1}, style]}>
          <Text style={{color: selectedValue ? 'black' : Colors.greyText1}}>
            {selectedValue ? selectedValue : placeholder}
          </Text>
          <Icon name={'menu-down'} size={25} color={Colors.greyText1} />
        </View>
        <Modal visible={visible} animationType="none" transparent={true}>
          <TouchableWithoutFeedback onPress={() => setVisible(false)}>
            <View style={styles.modal}>
              <View style={styles.pickerContent}>
                <FlatList
                  data={items}
                  renderItem={({item, index}) => (
                    <TouchableOpacity
                      onPress={() => {
                        onSelectedItem(item);
                        setVisible(false);
                      }}
                      style={styles.pickerItem}>
                      <Icon
                        name={
                          selectedValue == item.text
                            ? 'circle-slice-8'
                            : 'circle-outline'
                        }
                        size={25}
                        color={Colors.greyText1}
                      />
                      <Regular14
                        color={Colors.greyText1}
                        style={{marginLeft: 10}}>
                        {item.text}
                      </Regular14>
                    </TouchableOpacity>
                  )}
                />
              </View>
            </View>
          </TouchableWithoutFeedback>
        </Modal>
      </View>
    </TouchableWithoutFeedback>
  );
};

export default PickerSelect;
