import {StyleSheet} from 'react-native';
import Colors from '../../../styles/colors';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    // overflow: 'hidden',
    borderWidth: 1,
    borderColor: Colors.greyText1,
    justifyContent: 'space-between',
    borderRadius: 5,
    padding: 9,
  },
  modal: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.6)',
    width: '100%',
    height: '100%',
  },
  pickerContent: {
    backgroundColor: Colors.white,
    paddingHorizontal: 10,
    borderRadius: 5,
    width: '90%',
    maxHeight: '80%',
  },
  pickerItem: {
    padding: 15,
    borderBottomWidth: 1,
    borderBottomColor: Colors.greyBackground,
    flexDirection: 'row',
    alignItems: 'center',
  },
});

export default styles;
