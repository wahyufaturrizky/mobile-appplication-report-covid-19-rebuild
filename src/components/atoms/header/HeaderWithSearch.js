import React from 'react';
import {View, TouchableOpacity, Platform} from 'react-native';
import styles from './HeaderSearchStyle';
import {Regular16} from '../typography/typography';
import Colors from '../../../styles/colors';
import FormSearch from '../textInput/NormalTextInput';
import Icon from '../icons/Icon';

const HeaderSearch = ({
  backgroundColor,
  title,
  state,
  that,
  value,
  onChangeText,
}) => {
  return (
    <View
      style={[
        styles.container,
        {
          backgroundColor: backgroundColor
            ? backgroundColor
            : Colors.primaryMain,
        },
      ]}>
      <Regular16
        style={{color: Colors.white, marginTop: -10, marginBottom: 10}}>
        {title}
      </Regular16>
      <FormSearch
        onChangeText={onChangeText}
        value={value}
        radius={23}
        placeholder={'Search Here'}
        iconRight={'search1'}
        iconColor={Colors.greyText2}
      />
    </View>
  );
};

export default HeaderSearch;
