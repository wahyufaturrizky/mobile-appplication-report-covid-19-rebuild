import {StyleSheet} from 'react-native';
import Colors from '../../../styles/colors';

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 10,
    paddingVertical: 15,
    backgroundColor: Colors.primaryMain,
  },
  viewSearch: {
    width: '100%',
    paddingVertical: 7,
    paddingHorizontal: 10,
    backgroundColor: Colors.white,
    flexDirection: 'row',
    borderRadius: 23,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
});

export default styles;
