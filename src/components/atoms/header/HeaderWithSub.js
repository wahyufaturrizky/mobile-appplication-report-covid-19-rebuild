import React from 'react';
import {View, Text, TouchableOpacity, Image, Platform} from 'react-native';
import styles from './HeaderStyle';
import {Regular16, Regular12} from '../typography/typography';
import Colors from '../../../styles/colors';
import Icon from '../icons/Icon';

const HeaderWithSub = ({
  backgroundColor,
  title,
  subtitle,
  onBack,
  rightItem,
}) => {
  return (
    <View style={[styles.container, backgroundColor && {backgroundColor}]}>
      <View style={styles.left}>
        {onBack && (
          <TouchableOpacity onPress={onBack}>
            {Platform.OS == 'android' ? (
              <Icon name="arrow-back" color={Colors.primaryCard} />
            ) : (
              <Icon name="arrow-back-ios" color={Colors.primaryCard} />
            )}
          </TouchableOpacity>
        )}
      </View>
      <View style={styles.center}>
        <Regular16 color={Colors.primaryCard} style={{alignSelf: 'center'}}>
          {title}
        </Regular16>
        <Regular12 color={Colors.primaryCard} style={{alignSelf: 'center'}}>
          {subtitle}
        </Regular12>
      </View>
      <View style={styles.right}>{rightItem && rightItem()}</View>
    </View>
  );
};

export default HeaderWithSub;
