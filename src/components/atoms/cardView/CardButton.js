import React from 'react';
import {TouchableOpacity, View} from 'react-native';
import styles from './CardButtonStyle';
import Icon from '../icons/Icon';
import styleShadow from '../../../styles/shadow';

const CardButton = ({
  children,
  onPress,
  radius,
  paddingBottom,
  marginBottom,
  marginTop,
  borderColor,
  borderWidth,
  width,
}) => {
  return (
    <View
      style={[
        styles.container,
        styleShadow.shadow,
        {
          width: width,
          borderColor: borderColor ? borderColor : null,
          borderWidth: borderWidth ? borderWidth : 0,
          marginBottom: marginBottom ? marginBottom : 5,
          borderRadius: radius,
          paddingBottom: paddingBottom ? paddingBottom : 0,
          marginTop: marginTop ? marginTop : 0,
        },
      ]}
      onPress={onPress}>
      {children}
    </View>
  );
};

export default CardButton;
