import React from 'react';
import {ImageBackground, View} from 'react-native';
import LoginContent from '../../molecules/auth/LoginContent';
import {Regular18} from '../../atoms/typography/typography'

const LoginComponent = ({
  handleForm,
  form,
  presLogin
}) => {
  return (
   <ImageBackground style={{flex: 1}} resizeMode='cover' source={require('../../../assets/image/background-forgot-password.jpg')}>
      <View style={{flex: 0.8, justifyContent: 'center', alignItems: 'center'}}>
        <LoginContent presLogin={presLogin} form={form} handleForm={handleForm} />
      </View>
      <View style={{flex: 0.2, justifyContent: 'center', alignItems: 'center'}}>
        <View style={{
          alignSelf: 'center',
          // paddingVertical: 10
          }}>
          <Regular18 color="#FFF" style={{alignSelf: 'center', marginBottom: 8}}>Harmoniintegra@2020</Regular18>
          <Regular18 color="#FFF" style={{alignSelf: 'center'}}>V.1.0</Regular18>
        </View>
      </View>
   </ImageBackground>
  );
};

export default LoginComponent;
