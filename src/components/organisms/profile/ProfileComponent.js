import React from 'react';
import {View, Text} from 'react-native';
import styles from './style';
import ProfileContent from '../../molecules/profile/ProfileContent';

const ProfileComponent = ({
  screenProps,
}) => {
  return (
    <View style={styles.container}>
      <ProfileContent />
    </View>
  );
};

export default ProfileComponent;
