import React from 'react';
import {View, Text} from 'react-native';
import styles from './style';
import HomeContent from '../../molecules/home/HomeContent';

const HomeComponent = ({
  screenProps,
}) => {
  return (
    <View style={styles.container}>
      <HomeContent />
    </View>
  );
};

export default HomeComponent;
