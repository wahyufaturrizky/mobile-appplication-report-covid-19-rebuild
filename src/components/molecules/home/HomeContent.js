import React from 'react';
import {View, ImageBackground, Dimensions, Image, StyleSheet, TouchableOpacity} from 'react-native';
import {Regular18, Bold16} from '../../atoms/typography/typography'
import Colors from '../../../styles/colors'

const HomeContent = ({
  screenProps,
}) => {
  return (
    <View style={{flex: 1}}>
      <View style={{flex: 0.6}}>
        <ImageBackground source={require('../../../assets/image/tulungagung.jpg')}
          style={{flex: 1}} resizeMode='contain'
        >
          <Image source={require('../../../assets/image/logo-covid-header.png')} style={styles.imgHeader} />
        </ImageBackground>
      </View>
      <View style={styles.viewBtn}>
        <View style={{paddingHorizontal: 10}}>
          <TouchableOpacity activeOpacity={0.7} style={styles.btn}>
            <Regular18 color={'#f44336'} style={{fontWeight: 'bold'}}>Screening</Regular18>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  btn: {
    width: 140,
    height: 140,
    borderRadius: 140/2,
    borderColor: Colors.orange,
    borderWidth: 6,
    justifyContent: 'center',
    alignItems: 'center'
  },
  viewBtn: {
    flex: 0.4, 
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.blueDeep
  },
  imgHeader: {
    marginTop: 8, 
    marginLeft: 5, 
    width: Dimensions.get('window').width * 0.4,
    height: 45, 
    resizeMode: 'contain'
  }
})

export default HomeContent;
