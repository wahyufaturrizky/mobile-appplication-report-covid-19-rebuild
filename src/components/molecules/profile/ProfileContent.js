import React from 'react';
import {View, Text} from 'react-native';
import {Regular14} from '../../atoms/typography/typography'
import Header from '../../atoms/header/Header'

const ProfileContent = ({
  screenProps,
}) => {
  return (
    <View>
      <Header title='My Profile' />
    </View>
  );
};

export default ProfileContent;
