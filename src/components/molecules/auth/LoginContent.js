import React from 'react';
import {View, Image, StyleSheet, TextInput, TouchableOpacity, Dimensions} from 'react-native';
import {Regular18, Regular14} from '../../atoms/typography/typography'
import Colors from '../../../styles/colors'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Button from '../../atoms/button/Button'

const LoginContent = ({
  handleForm,
  form,
  presLogin
}) => {
   return (
      <View>
         <Image style={{marginBottom: 30, alignSelf: 'center', width: Dimensions.get('window').width * 0.8, height: 90}} source={require('../../../assets/image/logo-besar-2-Putih.png')} />
         <View style={styles.form}>
            <View style={styles.formEmail}>
               <View style={styles.icon}>
                  <Icon
                     name={'cellphone'}
                     size={25}
                     color={Colors.white}
                  />
               </View>
               <View style={{width: '80%', marginLeft: 5}}>
                  <TextInput
                     value={form.email}
                     placeholder='No HP'
                     // keyboardType='number-pad'
                     onChangeText={(text) => handleForm('email', text)}
                  />
               </View>
            </View>
            <View style={styles.formEmail}>
               <View style={styles.icon}>
                  <Icon
                     name={'lock-outline'}
                     size={25}
                     color={Colors.white}
                  />
               </View>
               <View style={{width: '80%', marginLeft: 5}}>
                  <TextInput
                     value={form.password}
                     secureTextEntry={true}
                     placeholder='Password'
                     onChangeText={(text) => handleForm('password', text)}
                  />
               </View>
            </View>
            <View style={styles.viewForgotPW}>
               <TouchableOpacity style={{padding: 3}}>
                  <Regular14 color={Colors.green1}>Lupa Password ?</Regular14>
               </TouchableOpacity>
            </View>
            <Button onPress={presLogin} width={'100%'} style={styles.viewButton} backgroundColor={Colors.orange} rounded>
               <Regular18 color={Colors.white}>LOGIN</Regular18>
            </Button>
         </View>
      </View>
   );
};

const styles = StyleSheet.create({
   viewButton: {
      marginTop: 15,
      alignSelf: 'center',
      justifyContent: 'center',
      alignItems: 'center'
   },
   viewForgotPW: {
      marginTop: 15,
      width: '90%',
      alignSelf: 'center',
      flexDirection: 'row',
      justifyContent: 'flex-end'
   },
   form: {
      width: '90%',
      alignSelf: 'center',
      paddingHorizontal: 10,
      marginTop: 20
   },
   formEmail: {
      width: '100%',
      flexDirection: 'row',
      alignItems: 'center',
      backgroundColor: Colors.white,
      borderRadius: 23,
      paddingHorizontal: 8,
      marginTop: 20
   },
   icon: {
      width: 35, 
      height: 35, 
      borderRadius: 35/2, 
      justifyContent: 'center', 
      alignItems: 'center',
      backgroundColor: Colors.orange
   }
})

export default LoginContent;
