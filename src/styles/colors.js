const Colors = {

  //primary colour
  primaryMain: '#221EA2',
  primaryButton: '#FCD87',
  primaryCard: '#FFFFFF',
  primaryBackground: '#F1F3F6',
  primaryTicket: '#7679B6',

  //secondary colour
  secondaryAlertTime: '#E88D32',
  secondaryTogleFilter: '#7D86F0',
  secondaryTogleUtama: '#3631DB',
  secondaryButtonNavBarActive: '',
  secondaryDisable: '#BFBCC9',
  secondaryGrey: '#BCBBDB',

  //grey colour 1 -> 5 = dark -> light
  greyText1: '#6E6F8D',
  greyText2: '#919191',
  greyText3: '#9EA1AD',
  greyBackground: '#F1F3F6',
  grey1: '#D7D7E5',
  grey2: '#D0D1E3',

  //blue colour 1 -> 5 = dark -> light
  blue1: '#0E0B64',
  blue2: '#181380',
  blue3: '#221EA2',
  blue4: '#413DAD',
  blue5: '#605CBD',
  blue6: '#9FACFF',
  blueLink: '#0275d8',
  blue7: '#8885EF',
  blue8: '#B9BDD8',
  blue9: '#4350C0',
  blue10: '#6F74E0',
  blueText1: '#483CD9',
  blueText2: '#4147BE',
  blueLogout: '#6F93F9',
  blueDeep: '#283593',

  //yellow colour 1 -> 5 = dark -> light
  yellow1: '#BA9227',
  yellow2: '#DAB44A',
  yellow3: '#FCD876',
  yellow4: '#FFE59D',
  yellow5: '#FFF0C6',
  orange: '#ff6f00',

  //brown colour 1 -> 5 = dark -> light
  brown1: '#9C4E02',
  brown2: '#C46B12',
  brown3: '#E88D32',
  brown4: '#FCAA5A',
  brown5: '#FFC184',
  brown6: '#454545',

  warning: '#E88D32',

  //white colour
  white: '#FFFFFF',

  //green
  green1: '#12BC93',

  //blue text input
  textInputColor: '#2E3283',
  borderBottomColor: 'rgba(187, 188, 221, 0.4)',
};

export default Colors;
