import React, { Component } from 'react';
import { Text, View } from 'react-native';
import HomeComponent from '../../components/organisms/home/HomeComponent';

export default class Home extends Component {
   render() {
      return (
         <View style={{flex: 1}}>
            <HomeComponent />
         </View>
      )
   }
}
