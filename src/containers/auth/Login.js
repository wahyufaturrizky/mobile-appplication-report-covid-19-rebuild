import React, {useState, useEffect} from 'react'
import {View, Text, Alert} from 'react-native'
import {Actions} from 'react-native-router-flux'
import { useSelector, useDispatch } from "react-redux"
import {authAction} from '../../redux/actions/actions'
import Loader from '../../components/atoms/loader/Loader'

import LoginComponent from '../../components/organisms/auth/LoginComponent'
const Login = () => {
   const { isLoggedIn, loginFailed } = useSelector(state => state.authReducer)
   const dispatch = useDispatch()
   const [form, setForm] = useState({
		email: "",
		password: "",
   })
   const [loading, setLoading] = useState(true)

   const handleForm = (name, value) => setForm({ ...form, [name]: value })
   const presLogin = () => {
      const { email, password } = form
      if(email && password) {
         setLoading(true)
         dispatch(authAction.loginProcess(form))
      } else {
         Alert.alert("Data belum lengkap")
      }
   }

   useEffect(() => {
      console.log('gagal', loginFailed);
      
		if (isLoggedIn) {
         setLoading(false)
         Actions.reset('intro')
		} else {
         setLoading(false)
      }

      if(loginFailed) {
         setLoading(false)
         Alert.alert(
            'Login gagal',
            'email atau password salah',
            [
              {text: 'OK', onPress: () => dispatch(authAction.reset_login_failed())},
            ],
            {cancelable: false},
          );
      }
	}, [isLoggedIn, loginFailed])

   return (
      <View style={{flex: 1}}>
         <Loader show={loading} />
         <LoginComponent
            // presLogin={() => Actions.intro()}
            presLogin={() => presLogin()}
            form={form}
            handleForm={(key, val) => handleForm(key, val)}
         />
      </View>
   )
}

export default Login;