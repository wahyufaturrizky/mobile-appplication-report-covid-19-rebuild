import React, { useEffect } from "react"
import { Image, View, ImageBackground, StyleSheet } from "react-native"
import moment from "moment"
import "moment/locale/id"
import { useSelector, useDispatch } from "react-redux"
import {Actions} from 'react-native-router-flux'
import {Regular30, Regular20, Regular18} from '../components/atoms/typography/typography'
import Colors from "../styles/colors"
import Button from '../components/atoms/button/Button'

const Intro = () => {
   const { isLoggedIn, userData } = useSelector(state => state.authReducer)
   const today = moment(new Date())
   console.log('userdata', userData);
   
   const getHumanTime = () => {
		const now = new Date()
		const hour = now.getHours()
		switch (true) {
		  case hour < 12:
			return "Selamat Pagi";
		  case hour < 15:
			return "Selamat Siang";
		  case hour < 18:
			return "Selamat Sore";
		  default:
			return "Selamat Malam";
		}
   }
   
   return (
      <ImageBackground
         style={styles.container} resizeMode='cover'
         source={require('../assets/image/background-forgot-password.jpg')}
      >
         <Regular30 color={Colors.white}>{today.format("dddd")}</Regular30>
         <Regular30 color={Colors.white}>{today.format("DD MMMM YYYY")}</Regular30>
         <Regular20 color={Colors.white} style={{marginVertical: 15}}>{getHumanTime()}</Regular20>
         <Regular30 color={Colors.white} style={{fontWeight: 'bold'}}>{userData.name}</Regular30>
         <Regular20 color={Colors.white} style={{marginTop: 20}}>Selamat Bertugas</Regular20>
         <Regular20 color={Colors.white}>Selalu jaga kesehatan</Regular20>
         <View style={{marginTop: 15, width: '100%'}}>
            <Button onPress={() => Actions.tabbar()} width={'90%'} style={styles.viewButton} backgroundColor={Colors.orange} rounded>
               <Regular18 color={Colors.white}>LANJUTKAN</Regular18>
            </Button>
         </View>
      </ImageBackground>
   )

}

const styles = StyleSheet.create({
   viewButton: {
      marginTop: 15,
      alignSelf: 'center',
      justifyContent: 'center',
      alignItems: 'center'
   },
   container: {
      flex: 1, 
      justifyContent: 'center', 
      alignItems: 'center'
   }
})

export default Intro;