import React, { Component } from 'react'
import { Text, View } from 'react-native'
import ProfileComponent from '../../components/organisms/profile/ProfileComponent'

export default class Profile extends Component {
   render() {
      return (
         <View style={{flex: 1}}>
            <ProfileComponent />
         </View>
      )
   }
}
